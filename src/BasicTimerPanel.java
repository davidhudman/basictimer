/**
 * BasicTimerPanel class
 * Author: David Hudman
 * Date: 11/1/14
 */

// These are the classes that we have to import that other people have created that will help us create the program
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Our class is going to "extend" the JPanel class. Therefore, it will contain all the 
 * methods and variables associated with the JPanel class and the stuff that we had to it below.
 * @author David Hudman
 */
public class BasicTimerPanel extends JPanel {
	
	Stopwatch s = new Stopwatch();	// This creates a Stopwatch object based on the class that we created with the Stopwatch.java file
	
	/**This is the class constructor that runs when the class is initialized*/
	BasicTimerPanel()
	{
		drawBasicPanel();	// A function I created. See decription below.
		
		// These three lines set up the button that we have on the panel
		JButton button = new JButton("Start Stopwatch");	// Initializes the button with the text that will be on it.
		button.addActionListener(new ButtonListener());		// Adds a listener function that will listen for when the button is clicked. The ButtonListener class below defines what happens when the button is clicked
		add (button);										// Adds the button that we initialized to the panel
		
		s.start();											// Starts the stopwatch object that we created above. Since we're calling the start() function in the constructor, the stopwatch appears to start "automatically" when the program opens
	}
	/** Not sure exactly how this works. I think it might be a function that is continuously called over and over again. As soon as it finishes, it is called again.*/
	public void paintComponent (Graphics page)
	{
		super.paintComponent(page);							// Not sure what this does either.
		page.setColor(Color.red);							// This sets the color of the font to the color that we pass to it. In this case the color is Color.red
		
		drawBasicPanel();									// A function I created. See decription below.
		
		if (s.getStartTime()>0)	// This if statement isn't currently necessary, but it might be useful you had a component that stops and resets the timer
		{
			page.drawString(s.getEstimatedTimeString(), 0, 200);	// Apparently, the Graphics class object that was passed to our paintComponent function has a method called drawString() that will draw any string you pass to it on the panel at the location (x,y) coordinates that you specify. We get the string from the Stopwatch class method getEstimatedTimeString()
		}
		
		/* This is a try / catch statement. To be honest, I'm not sure why Java requires that I use it, but the whole purpose of this section of code is to get the thread to sleep.
		 * In other words, "try" to do what's in the try section. If it works, skip the catch section. If it doesn't work, execute the catch section.*/
		try {
			Thread.sleep(50);	// This basically just allows the processor to sleep for a certain amount of time between updating the visual timer so that it isn't using up all your processing power by asking it to update the screen constantly
		} catch (InterruptedException e) {
			e.printStackTrace(); // If there is an error, this section will "catch" it and throw the error "printStackTrace()"...not exactly sure how all that works. It never throws an error.
		}
	}
	
	private class ButtonListener implements ActionListener
	{
		public void actionPerformed (ActionEvent event)
		{
			s.start();			// Whenever the button is clicked, the start() method from the stopwatch class is called
		}
	}
	
	/** Just draw a basic panel with a black background, large font, and a big enough size to hold the timer numbers.*/
	public void drawBasicPanel()
	{
		setPreferredSize (new Dimension(950, 200)); // size of the panel
		setBackground (Color.black); 				// color of the panel background, which is black
		setFont (new Font("Arial", Font.BOLD, 160)); // font for text printed on panel
	}
}
