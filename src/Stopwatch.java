import java.text.DecimalFormat;



public class Stopwatch
{
    private long startTime;										// Holds the value in milliseconds of when the stopwatch was started
    DecimalFormat twoDigits = new DecimalFormat("00");			// This helps us format strings into numbers that always display with two digits
    DecimalFormat threeDigits = new DecimalFormat("000");		// This helps us format strings into numbers that always display with three digits
    
    /**Starts the Stopwatch object running so that it keeps track of time */
    void start()
    {
    	startTime = System.currentTimeMillis();
    }
    
    /**
     * Asks for the time in milliseconds that the Stopwatch object was started
     * @return long startTime
     */
    public long getStartTime()
    {
    	return startTime;
    }
    
    /**
     * Asks for the time in milliseconds that the Stopwatch object has been running. This is derived by subtracting the startTime from the System.currentTimeMillis() which is a function that Java has built in that can ask for the current time in milliseconds
     * @return (long) System.currentTimeMillis() - startTime
     */
    public long getEstimatedTime()
    {
    	return System.currentTimeMillis() - startTime;
    }
    
    /**
     * Asks for the time that the Stopwatch object has been running and gives it back to you as a String object so it can easily be displayed
     * @return (String) the time Stopwatch object has been running
     */
    public String getEstimatedTimeString()
    {
    	/**
    	 * This is returning a String of the current time, so we need to use the String class's valueOf() method to get it into that format.
    	 * If you look at the first time that String.valueOf() is used, you'll see that we use that twoDigits variable that we created above.
    	 * the twoDigits variable is of the type DecimalFormat. That DecimalFormat class provides methods like DecimalFormat.format().
    	 * That method will format the number you give it into whatever format you specify above. In our case, this is "00"...aka "two digits".
    	 * The other valueOf methods follow the same logic.
    	 */
    	return (String.valueOf(twoDigits.format(getHours())) + ":" + String.valueOf(twoDigits.format(getMinutes())) + ":" + String.valueOf(twoDigits.format(getSeconds())) + "." + String.valueOf(threeDigits.format(getMilliSeconds())));
    }
    
    /**
     * Asks for the milliseconds since the Stopwatch object started and uses the modulus (%) operator to get the remainder after we divide by 1000 because we want a number between 0 and 999 to be displayed because 1000 milliseconds would equal one second and we wouldn't want it to be displayed that the Stopwatch has been running for 1000 milliseconds. The other functions follow similar logic related to the remainder of division operations 
     * @return (long) milliseconds since stopwatch started
     */
    public short getMilliSeconds()
    {
    	return (short)((getEstimatedTime()) % 1000);
    }
    
    /**
     * Asks for the seconds since the Stopwatch object started. See the comments above getMilliSeconds to help you understand the logic.
     * @return (long) seconds since stopwatch started
     */
    public byte getSeconds()
    {
    	return (byte)(((getEstimatedTime()) / 1000) % 60);
    }
    
    /**
     * Asks for the minutes since the Stopwatch object started. See the comments above getMilliSeconds to help you understand the logic.
     * @return (long) minutes since stopwatch started
     */
    public byte getMinutes()
    {
    	return (byte)(((getEstimatedTime()) / 60000) % 60);
    }
    
    /**
     * Asks for the hours since the Stopwatch object started. See the comments above getMilliSeconds to help you understand the logic.
     * @return (long) hours since stopwatch started
     */
    public long getHours()
    {
    	return (getEstimatedTime()) / 3600000;
    }
}