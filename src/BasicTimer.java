import javax.swing.JFrame; // This just import that JFrame class that we need

/**
 * This is the class that contains the main() function that Java looks for to actually run your program
 * @author David Hudman
 *
 */
public class BasicTimer {

	public static void main(String[] args) {
		
		JFrame frame = new JFrame("Timer");						// This is the standard window frame that contains the red "x" to close out your program, minimize, and maximize. We just created the JFrame object and it will say "Timer" at the top of the window
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	// I'm not sure what this does. It looks like it just lets the program end when you close the window
        
        frame.add(new BasicTimerPanel());						// This adds the panel that we created with the BasicTimerPanel.java file to the JFrame
        frame.pack();											// Not sure what this does
        frame.setVisible(true);									// I think this just makes the JFrame object visible
	}

}
